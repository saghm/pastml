# PasTML

PasTML is a static frontend providing pastebin-like functionality without needing any backend
beyond hosting the files.

## Pronunciation

Originally, I intended it to be pronounced "paste tee em ell" as a pun on "paste" and "HTML". However,
from staring at the word too much while working on this I now mentally read it as "pasty meal".
You're free to use either, or anything else you can think of that you like better.

# How does it work?

The code pasted into the page for creating the snippets to share is gzipped and encoded into base64
and then put into the URL querystring. When showing a snippet, the querystring value is decoded from
base64 and then decompressed before displaying.

PasTML leans heavily on existing libraries for almost all of the non-trivial frontend functionality:

* [wasm-gzip](https://github.com/ColinTimBarndt/wasm-gzip) is used for compressing and decompressing
the code snippets ([license: Apache-2.0](https://github.com/ColinTimBarndt/wasm-gzip/blob/473872cb4757be2f16c618d8c9eef589012ad216/Cargo.toml#L5))
* [highlight.js](https://github.com/highlightjs/highlight.js) is used to detect the language and 
apply syntax highlughting when displaying the snippet ([license: BSD 3-Clause](https://github.com/highlightjs/highlight.js/blob/82688fad1880f0bf8bbd80b470cddaad61bbcc2d/LICENSE), included at /static/highlight/LICENSE)
* [highlightjs-line-numbers.js](https://github.com/wcoder/highlightjs-line-numbers.js/) is used to 
add line numbers to the snippet ([license: MIT](https://github.com/wcoder/highlightjs-line-numbers.js/))

Additionally, the code for base64 encoding and decoding the snippet is borrowed from [the MDN docs](https://developer.mozilla.org/en-US/docs/Glossary/Base64#solution_2_%E2%80%93_rewriting_atob_and_btoa_using_typedarrays_and_utf-8) ([license: CC0](https://github.com/mdn/content/blob/main/LICENSE.md#added-on-or-after-august-20-2010))

# Security implications

## URL privacy

Because the URLs of the snippets generated contain the code with only trivial obfuscation (for 
convenience purposes, not for security), publicly sharing the URL of a snippet should be considered
to be equivalent to publicly sharing the code itself. Any code that needs to be kept private (e.g.
due to being proprietary) or is not allowed to be distributed (e.g. for licensing reasons or legal
restrictions) should not be shared through URLs created with this tool except through channels where
 sharing the code itself would not be an issue.
 
 ## HTML hygeine
 
 Although the code displayed is parsed entirely from user input, the content of the HTML is set
 using [`textContent`](https://developer.mozilla.org/en-US/docs/Web/API/Node/textContent) to
 mitigate injection risks. However, the text does subsequently get parsed and updated by
 `highlight.js` (and the `highlightjs-line-numbers.js`), so only input data into this tool that
 you're comfortable being processed this way.
 
 ## Depdenency availability
 
 All script and style dependencies are checked in locally to the project, so there shouldn't be any
 issues due to not being able to reach external sources due to downtime, firewall rules, etc.
 
 # Usage
 
PasTML is intended to be used in conjuction with a static file server. Hosting the `static`
directory as a whole rather than just opening `index.html` in a browser is necessary due to the use
of ES6 module imports (which trigger CORS restrictions otherwise).

To test locally, I use [sfz](https://github.com/weihanglo/sfz). The `run` script in the root of the
repo demonstrates a possible invocation:

```
➜  cat run
#!/bin/sh

command -v sfz >/dev/null || (echo "sfz must be installed" >&2 && exit 1)
sfz static -r

➜  ./run
Files served on http://127.0.0.1:5000
```

Opening up `index.html` will show an empty form:

![Screenshot of empty page for creating snippet](demo/empty_create.png)

Paste or manually type the code you want to share in the form and hit the "Create" button:

![Screenshot of page for creating snippet with code pasted in](demo/index_create.png)

The snippet will be shown in a page with a URL that can be shared with anyone else with access to
a PasTML server (regardless of whether it's the same one used to generate it). The "Create new"
button will return to the form.

![Screenshot of formatted code snippet](demo/snippet.png)
